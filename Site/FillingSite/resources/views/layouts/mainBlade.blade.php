<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>filling.spectroom-studio.ru</title>
    {{-- Обязательный токен --}}
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {{-- Styles --}}
    <link href="{{ asset('lib/app/app.css')}}"  rel="stylesheet">
    {{-- Custom styles for this template --}}
    <link href="{{ asset('lib/mainBlade/assets_css/style.css')}}" rel="stylesheet">
    <link href="{{ asset('lib/mainBlade/assets_css/style-responsive.css')}}" rel="stylesheet">
    <link href="{{ asset('lib/font-awesome/css/font-awesome.css')}}" rel="stylesheet">
    <link href="{{ asset('lib/mainBlade/assets_css/barAndMenu.css')}}" rel="stylesheet">
    {{-- Scripts --}}
    <script src="{{ asset('lib/app/app.js')}}" defer></script>
    {{-- jquery and bootstrap scripts --}}
    <script src="{{ asset('lib/jquery/jquery.min.js')}}"></script>
    <script src="{{ asset('lib/bootstrap/js/bootstrap.min.js')}}"></script>
    {{-- mainMenu scripts --}}
    <script src="{{ asset('lib/mainBlade/assets_js/jquery.dcjqaccordion.2.7.js')}}" defer></script>
    <script src="{{ asset('lib/mainBlade/assets_js/jquery.scrollTo.min.js')}}" defer></script>
    <script src="{{ asset('lib/mainBlade/assets_js/jquery.nicescroll.js')}}"></script>
    {{-- mainMenu scripts --}}
    <link rel="shortcut icon" href="/images/graph.ico" type="image/x-icon">
</head>
<body>
<div id="app"></div>
{{-- Страница предзагрузки css --}}
<link href="{{ asset('lib/mainBlade/preloader/preloader.css')}}"  rel="stylesheet">
{{-- Страница предзагрузки html --}}
<div class="preloader">
    <div class="preloader__loader">
        <img src="/images/loader6.gif" alt="" />
    </div>
</div>
{{-- Страница предзагрузки js --}}
<script type="text/javascript" src="{{ asset('lib/mainBlade/preloader/preloader.js')}}"></script>

{{-- Подключение меню --}}
@include('layouts.mainMenu')

{{-- Выпадающие сообщения --}}
<section class="container-fluid">
    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-3 " style="position:static">
            <div class="pull-right" id="toast_container"></div>
        </div>
    </div>
</section>

{{-- Главный контейнер 1 --}}
<section id="main-content" >
    <section class="wrapper">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 main-chart">
                @yield('css2')
                @yield('content2')
                @yield('js2')
            </div>
        </div>
    </section>
</section>

{{-- Главный контейнер 2--}}
<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-lg-11 main-chart">
                @yield('content1')
            </div>
            <div class="col-lg-1">
                @yield('rightBar1')
            </div>
        </div>
    </section>
</section>

@yield('scripts')

{{-- Страница предзагрузки css --}}
<link href="{{ asset('lib/mainBlade/preloader/preloader.css')}}"  rel="stylesheet">
{{-- Страница предзагрузки html --}}
<div class="preloader">
    <div class="preloader__loader">
        <img src="/images/loader6.gif" alt="" />
    </div>
</div>
{{-- Страница предзагрузки js --}}
<script type="text/javascript" src="{{ asset('lib/mainBlade/preloader/preloader.js')}}"></script>

{{-- mainMenu scripts --}}
<script type="text/javascript" src="{{ asset('lib/mainBlade/assets_js/common-scripts.js')}}" ></script>

</body>
</html>
