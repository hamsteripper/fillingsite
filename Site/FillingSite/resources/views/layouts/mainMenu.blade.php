<!--Шапка-->
<nav id="navHeader" class="header black-bg navbar navbar-toggleable-md navbar-light matovoye-steklo">
    <!--Управление навигационным меню-->
    <div class="navbar-brand">
        <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
    </div>
    <!--Logo-->
    <a class="logo top" style="font-size: 28px"><b>my<span>site</span></b></a>
    <!--Меню текущего пользователя-->
    <div class="top-menu">
        <ul class="nav  pull-right top-menu">
            <li>
                <!--Отображение меню пользователя-->
                @guest
                @else
                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                    {{ Auth::user()->name }} <span class="caret"></span>
                </a>
                @endguest
                <!--Взаимодействие с меню пользователя-->
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </li>
        </ul>
    </div>
</nav>

<!--Панель-->
{{--<aside>--}}
<!--Бокавая панель-->
<div id="sidebar" class="matovoye-steklo" style="margin-left: 0px;">
    <!--Главное меню-->
    <ul id="nav-accordion" class="sidebar-menu"  >
       <!--Подменю базы данных устройств-->
        <li class="sub-menu">
            <a href="javascript:;">
                <i class="fa fa-database"></i>
                <span>База сотрудников</span>
            </a>
            <ul class="sub1">
                <li><a href="/FillingEditor/sitesUsersSite">Добавление</a></li>
            </ul>
            <ul class="sub1">
                <li><a href="/FillingEditor/sitesUser">Редактирование пользователей</a></li>
            </ul>
            <ul class="sub1">
                <li><a href="/FillingEditor/site">Редактирование сайтов</a></li>
            </ul>
        </li>
    </ul>
</div>
{{--</aside>--}}

<script>
    (function($){
        var $pathURL = document.location.pathname;
        var els = document.querySelectorAll('a[href=\"'+$pathURL+'\"]');
        if(els[0].parentNode.parentNode.parentNode.tagName !== undefined &&
            els[0].parentNode.parentNode.parentNode.tagName === "DIV"){
            els[0].classList.add('active');
        }else if(els[0].parentNode.parentNode.parentNode.tagName !== undefined &&
            els[0].parentNode.parentNode.parentNode.tagName === "LI"){
            var childDivs = els[0].parentNode.parentNode.parentNode.getElementsByTagName("A");
            childDivs[0].classList.add('active');
            els[0].classList.add('active');
        }
    })(jQuery);
</script>
