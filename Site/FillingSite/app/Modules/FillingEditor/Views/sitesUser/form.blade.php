<div class="container h-100">
    <div class="row h-100 justify-content-center align-items-center">
        {{--<div class="col-md-8">--}}
            {{--<div class="card">--}}
{{--                <div class="container">--}}
{{--                    <div class="col-md-12 offset-md-2">--}}
                        <div class="card-img-top" >
                            <div class="card-body bg-light text-info">

                                <h1>{{isset($sitesUser)?'Редактирование':'Добавление'}} пользователя </h1>
                                <hr/>
                                @if(isset($sitesUser))
                                    {!! Form::model($sitesUser,['url' => url('FillingEditor/sitesUser').'/'.$sitesUser->userID, 'method'=>'put','id'=>'frm']) !!}
                                @else
                                    {!! Form::open(['url' => url('FillingEditor/sitesUser'), 'id'=>'frm']) !!}
                                @endif
                                {{--Fields--}}
                                {!!  Form::hidden('fromUrl', isset($fromUrl)?$fromUrl:"FillingEditor/sitesUser") !!}
                                <div class="form-group row required">
                                    {!! Form::label("type","username",["class"=>"col-form-label col-md-3 col-lg-2"]) !!}
                                    <div class="col-md-8">
                                        {!! Form::text("username",null,["class"=>"form-control".($errors->has('username')?" is-invalid":""),"autofocus",'placeholder'=>'username']) !!}
                                        <span id="error-type" class="invalid-feedback"></span>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-md-3 col-lg-2"></div>
                                    <div class="col-md-4">
                                        <a href="javascript:ajaxLoad('{{url('FillingEditor/sitesUser')}}')" class="btn btn-danger">Back</a>
                                        {!! Form::button("Save",["type" => "submit","class"=>"btn btn-primary"])!!}
                                    </div>
                                </div>

                            </div>
                        </div>
                        {!! Form::close() !!}
{{--                    </div>--}}
{{--                </div>--}}
            {{--</div>--}}
        {{--</div>--}}
    </div>
</div>
