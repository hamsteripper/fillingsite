<div class="container h-100">
    <div class="row h-100 justify-content-center align-items-center">
        {{--<div class="col-md-8">--}}
            {{--<div class="card">--}}
{{--                <div class="container">--}}
{{--                    <div class="col-md-12 offset-md-2">--}}
                        <div class="card-img-top" >
                            <div class="card-body bg-light text-info">

                                <h1>{{isset($SitesUsersSite)?'Редактирование':'Добавление'}} пользователя </h1>
                                <hr/>
                                @if(isset($SitesUsersSite))
                                    {!! Form::model($SitesUsersSite,['url' => url('FillingEditor/sitesUsersSite').'/'.$SitesUsersSite->id, 'method'=>'put','id'=>'frm']) !!}
                                @else
                                    {!! Form::open(['url' => url('FillingEditor/sitesUsersSite'), 'id'=>'frm']) !!}
                                @endif
                                {{--Fields--}}
                                {{--{{ Form::hidden('current_url', url()->current()) }}--}}
                                <div class="form-group row required">
                                    {!! Form::label("Username","Username:",["class"=>"col-form-label col-md-3 col-lg-2"]) !!}
                                    <div class="col-md-8">
                                        <select class="form-control" name="user_id">
                                            @foreach($sitesUsers as $user)
                                                @if(isset($SitesUsersSite))
                                                    @if($SitesUsersSite->user_id==$user->userID)
                                                        <option value="{{$user->userID}}" selected>{{$user->username}}</option>
                                                    @else
                                                        <option value="{{$user->userID}}">{{$user->username}}</option>
                                                    @endif
                                                @else
                                                    <option value="{{$user->userID}}">{{$user->username}}</option>
                                                @endif
                                            @endforeach
                                            <span id="error-model_name" class="invalid-feedback"></span>
                                        </select>

                                    </div>
                                    <div class="col-md-2">
                                        <a href="javascript:ajaxLoad('{{url('FillingEditor/sitesUser/create'.'?fromUrl='.Request::path())}}')" class="btn btn-info">Add user</a>
                                    </div>
                                </div>
                                <div class="form-group row required">
                                    {!! Form::label("Site","Site:",["class"=>"col-form-label col-md-3 col-lg-2"]) !!}
                                    <div class="col-md-8">
                                        <select class="form-control" name="site_id">
                                            @foreach($sites as $site)
                                                @if(isset($SitesUsersSite))
                                                    @if($SitesUsersSite->site_id==$site->siteID)
                                                        <option value="{{$site->siteID}}" selected>{{$site->site}}</option>
                                                    @else
                                                        <option value="{{$site->siteID}}">{{$site->site}}</option>
                                                    @endif
                                                @else
                                                    <option value="{{$site->siteID}}">{{$site->site}}</option>
                                                @endif
                                            @endforeach
                                            <span id="error-model_name" class="invalid-feedback"></span>
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="javascript:ajaxLoad('{{url('FillingEditor/site/create'.'?fromUrl='.Request::path())}}')" class="btn btn-info">Add site</a>
                                    </div>
                                </div>
                                <div class="form-group row required">
                                    {!! Form::label("type","login",["class"=>"col-form-label col-md-3 col-lg-2"]) !!}
                                    <div class="col-md-8">
                                        {!! Form::text("login",null,["class"=>"form-control".($errors->has('login')?" is-invalid":""),"autofocus",'placeholder'=>'login']) !!}
                                        <span id="error-type" class="invalid-feedback"></span>
                                    </div>
                                </div>
                                <div class="form-group row required">
                                    {!! Form::label("type","password",["class"=>"col-form-label col-md-3 col-lg-2"]) !!}
                                    <div class="col-md-8">
                                        {!! Form::text("password",null,["class"=>"form-control".($errors->has('password')?" is-invalid":""),"autofocus",'placeholder'=>'password']) !!}
                                        <span id="error-type" class="invalid-feedback"></span>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-md-3 col-lg-2"></div>
                                    <div class="col-md-4">
                                        <a href="javascript:ajaxLoad('{{url('FillingEditor/sitesUsersSite')}}')" class="btn btn-danger">Back</a>
                                        {!! Form::button("Save",["type" => "submit","class"=>"btn btn-primary"])!!}
                                    </div>
                                </div>

                            </div>
                        </div>
                        {!! Form::close() !!}
{{--                    </div>--}}
{{--                </div>--}}
            {{--</div>--}}
        {{--</div>--}}
    </div>
</div>
