<div class="container">
    {{--New/Back--}}
    <div class="float-right">
        <a href="{{url('/')}}" class="btn btn-danger">Back</a>
        <a href="javascript:ajaxLoad('{{url('FillingEditor/sitesUsersSite/create')}}')" class="btn btn-primary">New</a>
    </div>
{{--    <br>--}}
{{--    <br/>--}}
{{--    Search--}}
{{--    <div class="row">--}}
{{--        <div class="col-sm-5 form-group">--}}
{{--            <div class="input-group">--}}
{{--                <input class="form-control" id="searchType"--}}
{{--                       value="{{ request()->session()->get('searchType') }}"--}}
{{--                       onkeydown="if (event.keyCode == 13) ajaxLoad('{{url('FillingEditor/site')}}?searchType='+this.value)"--}}
{{--                       placeholder="Search" name="searchType"--}}
{{--                       type="text"/>--}}
{{--                <div class="input-group-btn">--}}
{{--                    <button type="submit" class="btn btn-warning" onclick="ajaxLoad('{{url('FillingEditor/site')}}?searchType='+$('#searchType').val())">Search</button>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}

    {{--Table--}}
    <table class="table table-bordered bg-light">
        <thead class="bg-dark" style="color: white">
        <tr>
            <th width="60px" style="vertical-align: middle;text-align: center">
{{--                <div class="input-group-btn">--}}
{{--                    <button type="submit" class="btn btn-warning" onclick="ajaxLoad('{{url('FillingEditor/site')}}?searchReset=1')">Сброс</button>--}}
{{--                </div>--}}
            </th>

            <th style="vertical-align: middle">
                <input class="form-control" id="searchUsername"
                       value="{{ request()->session()->get('searchUsername') }}"
                       onkeydown="if (event.keyCode == 13) ajaxLoad('{{url('FillingEditor/sitesUsersSite')}}?searchUsername='+this.value)"
                       placeholder="searchUsername" name="searchUsername"
                       type="text"/>
            </th>
            <th style="vertical-align: middle">
                <input class="form-control" id="searchSite"
                       value="{{ request()->session()->get('searchSite') }}"
                       onkeydown="if (event.keyCode == 13) ajaxLoad('{{url('FillingEditor/sitesUsersSite')}}?searchSite='+this.value)"
                       placeholder="searchSite" name="searchSite"
                       type="text"/>
            </th>
            <th style="vertical-align: middle">
                <input class="form-control" id="searchLogin"
                       value="{{ request()->session()->get('searchLogin') }}"
                       onkeydown="if (event.keyCode == 13) ajaxLoad('{{url('FillingEditor/sitesUsersSite')}}?searchLogin='+this.value)"
                       placeholder="searchLogin" name="searchLogin"
                       type="text"/>
            </th>
            <th style="vertical-align: middle">
                <input class="form-control" id="searchPassword"
                       value="{{ request()->session()->get('searchPassword') }}"
                       onkeydown="if (event.keyCode == 13) ajaxLoad('{{url('FillingEditor/sitesUsersSite')}}?searchPassword='+this.value)"
                       placeholder="searchPassword" name="searchPassword"
                       type="text"/>
            </th>
            <th style="vertical-align: middle">

            </th>
        </tr>
        <tr>
            <th width="60px" style="vertical-align: middle;text-align: center">No</th>
            <th style="vertical-align: middle">
                Username
            </th>
            <th style="vertical-align: middle">
                Site
            </th>
            <th style="vertical-align: middle">
                Login
            </th>
            <th style="vertical-align: middle">
                Password
            </th>
            <th style="vertical-align: middle">
                Action
            </th>
        </tr>
        </thead>
        <tbody>
        @php
            $i=1;
        @endphp
        @foreach($sitesUsersSite as $data)
            <tr>
                <th style="vertical-align: middle;text-align: center">{{$i++}}</th>
                <td style="vertical-align: middle">{{ $data->sitesUser->username }}</td>
                <td style="vertical-align: middle">{{ $data->site->site }}</td>
                <td style="vertical-align: middle">{{ $data->login }}</td>
                <td style="vertical-align: middle">{{ $data->password }}</td>
                <td style="vertical-align: middle" align="center">
                    <a class="btn btn-primary btn-sm"
                       title="Edit"
                       href="javascript:ajaxLoad('{{url('FillingEditor/sitesUsersSite/'.$data->id.'/edit')}}')">Edit
                    </a>
                    <input type="hidden" name="_method" value="delete"/>
                    <a class="btn btn-danger btn-sm" title="Delete"
                        href="javascript:if(confirm('Are you sure want to delete?')) ajaxDelete('{{url('FillingEditor/sitesUsersSite/'.$data->id)}}','{{csrf_token()}}')">
                        Delete
                    </a>
                </td>

            </tr>
        @endforeach
        </tbody>
    </table>
    {{--Next page--}}
    <nav>
        <ul class="pagination justify-content-end">
            {{$sitesUsersSite->links('vendor.pagination.bootstrap-4')}}
        </ul>
    </nav>
</div>
