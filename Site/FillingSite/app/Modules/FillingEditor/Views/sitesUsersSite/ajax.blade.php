@extends('layouts.mainBlade')

@section('css2')
    <style>
        /*.loading {*/
        /*    background: lightgrey;*/
        /*    padding: 15px;*/
        /*    position: fixed;*/
        /*    border-radius: 4px;*/
        /*    left: 50%;*/
        /*    top: 50%;*/
        /*    text-align: center;*/
        /*    margin: -40px 0 0 -50px;*/
        /*    z-index: 2000;*/
        /*    display: none;*/
        /*}*/

        a, a:hover {
            color: white;
        }

        .form-group.required label:after {
            content: " *";
            color: red;
            font-weight: bold;
        }
    </style>
@endsection

@section('content2')
    <div id="content">
        @include("FillingEditor::sitesUsersSite.index")
    </div>
{{--    <div class="loading">--}}
{{--        <i class="fa fa-refresh fa-spin fa-2x fa-fw"></i><br/>--}}
{{--        <span>Loading</span>--}}
{{--    </div>--}}
@endsection

@section('js2')
    <script src="{{ asset('dynamicModules/FillingEditor/assets_js/ajax-crud.js') }}"></script>
    <script src="{{ asset('dynamicModules/FillingEditor/assets_js/2c7a93b259.js') }}"></script>
@endsection
