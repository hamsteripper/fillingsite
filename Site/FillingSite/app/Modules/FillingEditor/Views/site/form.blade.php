<div class="container h-100">
    <div class="row h-100 justify-content-center align-items-center">
        {{--<div class="col-md-8">--}}
            {{--<div class="card">--}}
{{--                <div class="container">--}}
{{--                    <div class="col-md-12 offset-md-2">--}}
                        <div class="card-img-top" >
                            <div class="card-body bg-light text-info">

                                <h1>{{isset($site)?'Редактирование':'Добавление'}} сайта </h1>
                                <hr/>
                                @if(isset($site))
                                    {!! Form::model($site,['url' => url('FillingEditor/site').'/'.$site->siteID, 'method'=>'put','id'=>'frm']) !!}
                                @else
                                    {!! Form::open(['url' => url('FillingEditor/site'), 'id'=>'frm']) !!}
                                @endif
                                {{--Fields--}}
                                {!!  Form::hidden('fromUrl', isset($fromUrl)?$fromUrl:url()->current()) !!}

                                <div class="form-group row required">
                                    {!! Form::label("type","site",["class"=>"col-form-label col-md-3 col-lg-2"]) !!}
                                    <div class="col-md-8">
                                        {!! Form::text("site",null,["class"=>"form-control".($errors->has('site')?" is-invalid":""),"autofocus",'placeholder'=>'site']) !!}
                                        <span id="error-type" class="invalid-feedback"></span>
                                    </div>
                                </div>
                                <div class="form-group row required">
                                    {!! Form::label("type","url",["class"=>"col-form-label col-md-3 col-lg-2"]) !!}
                                    <div class="col-md-8">
                                        {!! Form::text("url",null,["class"=>"form-control".($errors->has('url')?" is-invalid":""),"autofocus",'placeholder'=>'url']) !!}
                                        <span id="error-type" class="invalid-feedback"></span>
                                    </div>
                                </div>
                                <div class="form-group row required">
                                    {!! Form::label("type","login_tag",["class"=>"col-form-label col-md-3 col-lg-2"]) !!}
                                    <div class="col-md-8">
                                        {!! Form::text("login_tag",null,["class"=>"form-control".($errors->has('login_tag')?" is-invalid":""),"autofocus",'placeholder'=>'login_tag']) !!}
                                        <span id="error-type" class="invalid-feedback"></span>
                                    </div>
                                </div>
                                <div class="form-group row required">
                                    {!! Form::label("type","login_search",["class"=>"col-form-label col-md-3 col-lg-2"]) !!}
                                    <div class="col-md-8">
                                        {!! Form::text("login_search",null,["class"=>"form-control".($errors->has('login_search')?" is-invalid":""),"autofocus",'placeholder'=>'login_search']) !!}
                                        <span id="error-type" class="invalid-feedback"></span>
                                    </div>
                                </div>
                                <div class="form-group row required">
                                    {!! Form::label("type","password_tag",["class"=>"col-form-label col-md-3 col-lg-2"]) !!}
                                    <div class="col-md-8">
                                        {!! Form::text("password_tag",null,["class"=>"form-control".($errors->has('login_search')?" is-invalid":""),"autofocus",'placeholder'=>'password_tag']) !!}
                                        <span id="error-type" class="invalid-feedback"></span>
                                    </div>
                                </div>
                                <div class="form-group row required">
                                    {!! Form::label("type","password_search",["class"=>"col-form-label col-md-3 col-lg-2"]) !!}
                                    <div class="col-md-8">
                                        {!! Form::text("password_search",null,["class"=>"form-control".($errors->has('login_search')?" is-invalid":""),"autofocus",'placeholder'=>'password_search']) !!}
                                        <span id="error-type" class="invalid-feedback"></span>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-md-3 col-lg-2"></div>
                                    <div class="col-md-4">
                                        <a href="javascript:ajaxLoad('{{url('FillingEditor/site')}}')" class="btn btn-danger">Back</a>
                                        {!! Form::button("Save",["type" => "submit","class"=>"btn btn-primary"])!!}
                                    </div>
                                </div>

                            </div>
                        </div>
                        {!! Form::close() !!}
{{--                    </div>--}}
{{--                </div>--}}
            {{--</div>--}}
        {{--</div>--}}
    </div>
</div>
