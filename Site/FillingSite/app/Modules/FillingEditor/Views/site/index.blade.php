<div class="container">
    {{--New/Back--}}
    <div class="float-right">
        <a href="{{url('/')}}" class="btn btn-danger">Back</a>
        <a href="javascript:ajaxLoad('{{url('FillingEditor/site/create'.'?fromUrl='.Request::path())}}')" class="btn btn-primary">New</a>
    </div>
{{--    <br>--}}
{{--    <br/>--}}
{{--    Search--}}
{{--    <div class="row">--}}
{{--        <div class="col-sm-5 form-group">--}}
{{--            <div class="input-group">--}}
{{--                <input class="form-control" id="searchType"--}}
{{--                       value="{{ request()->session()->get('searchType') }}"--}}
{{--                       onkeydown="if (event.keyCode == 13) ajaxLoad('{{url('FillingEditor/site')}}?searchType='+this.value)"--}}
{{--                       placeholder="Search" name="searchType"--}}
{{--                       type="text"/>--}}
{{--                <div class="input-group-btn">--}}
{{--                    <button type="submit" class="btn btn-warning" onclick="ajaxLoad('{{url('FillingEditor/site')}}?searchType='+$('#searchType').val())">Search</button>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}

    {{--Table--}}
    <table class="table table-bordered bg-light">
        <thead class="bg-dark" style="color: white">
        <tr>
            <th width="60px" style="vertical-align: middle;text-align: center">
{{--                <div class="input-group-btn">--}}
{{--                    <button type="submit" class="btn btn-warning" onclick="ajaxLoad('{{url('FillingEditor/site')}}?searchReset=1')">Сброс</button>--}}
{{--                </div>--}}
            </th>
            <th style="vertical-align: middle">
                <input class="form-control" id="searchSite"
                       value="{{ request()->session()->get('searchSite') }}"
                       onkeydown="if (event.keyCode == 13) ajaxLoad('{{url('FillingEditor/site')}}?searchSite='+this.value)"
                       placeholder="Search" name="searchSite"
                       type="text"/>
            </th>
            <th style="vertical-align: middle">
                <input class="form-control" id="searchUrl"
                       value="{{ request()->session()->get('searchUrl') }}"
                       onkeydown="if (event.keyCode == 13) ajaxLoad('{{url('FillingEditor/site')}}?searchUrl='+this.value)"
                       placeholder="Search" name="searchUrl"
                       type="text"/>
            </th>
            <th style="vertical-align: middle">
                <input class="form-control" id="searchLogin_tag"
                       value="{{ request()->session()->get('searchLogin_tag') }}"
                       onkeydown="if (event.keyCode == 13) ajaxLoad('{{url('FillingEditor/site')}}?searchLogin_tag='+this.value)"
                       placeholder="Search" name="searchLogin_tag"
                       type="text"/>
            </th>
            <th style="vertical-align: middle">
                <input class="form-control" id="searchLogin_search"
                       value="{{ request()->session()->get('searchLogin_search') }}"
                       onkeydown="if (event.keyCode == 13) ajaxLoad('{{url('FillingEditor/site')}}?searchLogin_search='+this.value)"
                       placeholder="Search" name="searchLogin_search"
                       type="text"/>
            </th>
            <th style="vertical-align: middle">
                <input class="form-control" id="searchPassword_tag"
                       value="{{ request()->session()->get('searchPassword_tag') }}"
                       onkeydown="if (event.keyCode == 13) ajaxLoad('{{url('FillingEditor/site')}}?searchPassword_tag='+this.value)"
                       placeholder="Search" name="searchPassword_tag"
                       type="text"/>
            </th>
            <th style="vertical-align: middle">
                <input class="form-control" id="searchPassword_search"
                       value="{{ request()->session()->get('searchPassword_search') }}"
                       onkeydown="if (event.keyCode == 13) ajaxLoad('{{url('FillingEditor/site')}}?searchPassword_search='+this.value)"
                       placeholder="Search" name="searchPassword_search"
                       type="text"/>
            </th>
            <th style="vertical-align: middle">

            </th>
        </tr>
        <tr>
            <th width="60px" style="vertical-align: middle;text-align: center">No</th>
            <th style="vertical-align: middle">
                Site
            </th>
            <th width="130px" style="vertical-align: middle">
                URL
            </th>
            <th width="130px" style="vertical-align: middle">
                login_tag
            </th>
            <th width="130px" style="vertical-align: middle">
                login_search
            </th>
            <th width="130px" style="vertical-align: middle">
                password_tag
            </th>
            <th width="130px" style="vertical-align: middle">
                password_search
            </th>
            <th width="130px" style="vertical-align: middle">
                Action
            </th>
        </tr>
        </thead>
        <tbody>
        @php
            $i=1;
        @endphp
        @foreach($sites as $site)
            <tr>
                <th style="vertical-align: middle;text-align: center">{{$i++}}</th>
                <td style="vertical-align: middle">{{ $site->site }}</td>
                <td style="vertical-align: middle">{{ $site->url }}</td>
                <td style="vertical-align: middle">{{ $site->login_tag }}</td>
                <td style="vertical-align: middle">{{ $site->login_search }}</td>
                <td style="vertical-align: middle">{{ $site->password_tag }}</td>
                <td style="vertical-align: middle">{{ $site->password_search }}</td>

                <td style="vertical-align: middle" align="center">
                    <a class="btn btn-primary btn-sm"
                       title="Edit"
                       href="javascript:ajaxLoad('{{url('FillingEditor/site/'.$site->siteID.'/edit')}}')">Edit
                    </a>
                    <input type="hidden" name="_method" value="delete"/>
                    <a class="btn btn-danger btn-sm" title="Delete"
                        href="javascript:if(confirm('Are you sure want to delete?')) ajaxDelete('{{url('FillingEditor/site/'.$site->siteID)}}','{{csrf_token()}}')">
                        Delete
                    </a>
                </td>

            </tr>
        @endforeach
        </tbody>
    </table>
    {{--Next page--}}
    <nav>
        <ul class="pagination justify-content-end">
            {{$sites->links('vendor.pagination.bootstrap-4')}}
        </ul>
    </nav>
</div>
