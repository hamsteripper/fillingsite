<?php

Route::group(
    [
        'prefix' => 'FillingEditor',
        'namespace' => 'App\Modules\FillingEditor\Controllers',
        'middleware' => ['web','auth'],
    ],
    function () {

        Route::get('/', 'MainController@index');

        Route::resource('site', 'SiteController');

        Route::resource('sitesUsersSite', 'SitesUsersSiteController');

        Route::resource('sitesUser', 'SitesUserController');

    }
);

Route::group(
    [
        'namespace' => 'App\Modules\FillingEditor\Controllers',
    ],
    function () {

        // Получение информации из базы данных
        Route::post('/GetSitesInfo', 'GetSitesInfoController@getData')->name('GetSitesInfo');

    }
);


