<?php

namespace App\Modules\FillingEditor\Models\DB;

use Illuminate\Database\Eloquent\Model;
use App\Modules\FillingEditor\Models\DB\Site;
use App\Modules\FillingEditor\Models\DB\SitesUser;

class SitesUsersSite extends Model
{
    public $timestamps = false;
//    protected $primaryKey = array('site_id', 'user_id');

    protected $fillable = [
        'user_id', 'site_id', 'login', 'password',
    ];

    public function site(){
        return $this->belongsTo('App\Modules\FillingEditor\Models\DB\Site', 'site_id', 'siteID');
    }

    public function sitesUser(){
        return $this->belongsTo('App\Modules\FillingEditor\Models\DB\SitesUser', 'user_id', 'userID');
    }

}
