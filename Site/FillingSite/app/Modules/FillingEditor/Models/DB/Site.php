<?php

namespace App\Modules\FillingEditor\Models\DB;

use Illuminate\Database\Eloquent\Model;
use App\Modules\FillingEditor\Models\DB\SitesUsersSite;

class Site extends Model
{
    public $timestamps = false;
    protected $primaryKey = 'siteID';

    protected $fillable = [
        'site', 'url', 'login_tag', 'login_search', 'password_tag', 'password_search',
    ];

    public function sites_users_sites(){
        return $this->hasMany('App\Modules\FillingEditor\Models\DB\SitesUsersSite', 'siteID', 'site_id');
    }

}
