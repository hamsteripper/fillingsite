<?php

namespace App\Modules\FillingEditor\Models\DB;

use Illuminate\Database\Eloquent\Model;
use App\Modules\FillingEditor\Models\DB\SitesUsersSite;

class SitesUser extends Model
{
    public $timestamps = false;
    protected $primaryKey = 'userID';

    public function sites_users_sites(){
        return $this->hasMany('App\Modules\FillingEditor\Models\DB\SitesUsersSite', 'userID', 'user_id');
    }
}
