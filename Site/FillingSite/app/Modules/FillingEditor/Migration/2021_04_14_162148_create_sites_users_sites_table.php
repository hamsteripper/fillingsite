<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSitesUsersSitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sites_users_sites', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned()->nullable(false);
            $table->bigInteger('site_id')->unsigned()->nullable(false);
            $table->string('login')->nullable(false);
            $table->string('password')->nullable(false);

            $table->foreign('user_id')->references('userID')->on('sites_users');
            $table->foreign('site_id')->references('siteID')->on('sites');

            $table->unique(['user_id','site_id']);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sites_users_sites');
    }
}
