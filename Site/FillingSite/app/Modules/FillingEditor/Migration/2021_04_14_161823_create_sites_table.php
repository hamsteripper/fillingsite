<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sites', function (Blueprint $table) {
            $table->bigIncrements('siteID');
            $table->string('site')->nullable(false);
            $table->string('url')->nullable(false);
            $table->string('login_tag')->nullable(false);
            $table->string('login_search')->nullable(false);
            $table->string('password_tag')->nullable(false);
            $table->string('password_search')->nullable(false);
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sites');
    }
}
