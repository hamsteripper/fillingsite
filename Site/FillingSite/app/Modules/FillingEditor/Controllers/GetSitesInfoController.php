<?php

namespace App\Modules\FillingEditor\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class GetSitesInfoController extends Controller
{

    public function index(){

    }

    public function getData(Request $request){

        $users = DB::select('SELECT username, site, url, login_tag, login_search, password_tag, password_search, login, password
                                    FROM sites_users, sites, sites_users_sites
                                    WHERE sites_users.userID=sites_users_sites.user_id
                                        AND sites.siteID=sites_users_sites.site_id
                                        AND sites_users.username = ?', [$request->username]);

        // Преобразование в array
        $users = json_decode(json_encode($users), true);

        $user_sites = array();
        $all_sites = array();

        foreach ($users as $user){
            $user_s = array();
            $user_s["site"] = $user["site"];
            $user_s["url"] = $user["url"];
            $user_s["login"] = $user["login"];
            $user_s["password"] = $user["password"];
            $user_sites[] = $user_s;

            $site_c = array();
            $site_c['url'] = $user["url"];
            $site_c['loginTag'] = $user["login_tag"];
            $site_c['loginSearch'] = $user["login_search"];
            $site_c['passwordTag'] = $user["password_tag"];
            $site_c['passwordSearch'] = $user["password_search"];
            $all_sites[] = $site_c;
        }

        return json_encode(["userData" => $user_sites, "siteData" => $all_sites]);
    }

}
