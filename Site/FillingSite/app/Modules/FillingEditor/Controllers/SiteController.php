<?php

namespace App\Modules\FillingEditor\Controllers;

//use App\Modules\DevicesDatabase\Models\dh_type;
use App\Modules\FillingEditor\Models\DB\Site;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
//use function Psy\debug;

class SiteController extends Controller {

    public function index(Request $request){

        $request->session()->put('searchSite', $request->has('searchSite') ? $request->get('searchSite') : ($request->session()->has('searchSite') ? $request->session()->get('searchSite') : ''));
        $request->session()->put('searchUrl', $request->has('searchUrl') ? $request->get('searchUrl') : ($request->session()->has('searchUrl') ? $request->session()->get('searchUrl') : ''));
        $request->session()->put('searchLogin_tag', $request->has('searchLogin_tag') ? $request->get('searchLogin_tag') : ($request->session()->has('searchLogin_tag') ? $request->session()->get('searchLogin_tag') : ''));
        $request->session()->put('searchLogin_search', $request->has('searchLogin_search') ? $request->get('searchLogin_search') : ($request->session()->has('searchLogin_search') ? $request->session()->get('searchLogin_search') : ''));
        $request->session()->put('searchPassword_tag', $request->has('searchPassword_tag') ? $request->get('searchPassword_tag') : ($request->session()->has('searchPassword_tag') ? $request->session()->get('searchPassword_tag') : ''));
        $request->session()->put('searchPassword_search', $request->has('searchPassword_search') ? $request->get('searchPassword_search') : ($request->session()->has('searchPassword_search') ? $request->session()->get('searchPassword_search') : ''));

        $sites = new Site();

        $searchSite = $request->session()->get('searchSite');
        $searchUrl = $request->session()->get('searchUrl');
        $searchLogin_tag = $request->session()->get('searchLogin_tag');
        $searchLogin_search = $request->session()->get('searchLogin_search');
        $searchPassword_tag = $request->session()->get('searchPassword_tag');
        $searchPassword_search = $request->session()->get('searchPassword_search');

        $sites = $sites
            ->where('site', 'like', '%' . $searchSite . '%')
            ->where('url', 'like', '%' . $searchUrl . '%')
            ->where('login_tag', 'like', '%' . $searchLogin_tag . '%')
            ->where('login_search', 'like', '%' . $searchLogin_search . '%')
            ->where('password_tag', 'like', '%' . $searchPassword_tag . '%')
            ->where('password_search', 'like', '%' . $searchPassword_search . '%')
            ->orderBy('siteID', 'desc')
            ->paginate(10);

        if ($request->ajax())
            return view("FillingEditor::site.index", compact('sites'));
        else
            return view('FillingEditor::site.ajax', compact('sites'));
    }

    // Создание новой строки
    public function create(Request $request){
        return view('FillingEditor::site.form', ['fromUrl' => $request->fromUrl]);
    }
    public function store(Request $request){
        $rules = [
            'site' => 'required',
            'url' => 'required',
            'login_tag' => 'required',
            'login_search' => 'required',
            'password_tag' => 'required',
            'password_search' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            Log::debug($validator->errors());
            return response()->json([
                'fail' => true,
                'errors' => $validator->errors()
            ]);
        }
        $sites = new Site();
        $sites->site = $request->site;
        $sites->url = $request->url;
        $sites->login_tag = $request->login_tag;
        $sites->login_search = $request->login_search;
        $sites->password_tag = $request->password_tag;
        $sites->password_search = $request->password_search;
        $sites->save();
        return response()->json([
            'fail' => false,
            'redirect_url' => url($request->fromUrl)
        ]);
    }

    // Редактирование текущей строки
    public function edit(Request $request, $id){
        return view('FillingEditor::site.form', ['site' => Site::find($id)]);
    }
    public function update(Request $request, $id){
            $rules = [
                'site' => 'required',
                'url' => 'required',
                'login_tag' => 'required',
                'login_search' => 'required',
                'password_tag' => 'required',
                'password_search' => 'required',
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                Log::debug($validator->errors());
                return response()->json([
                    'fail' => true,
                    'errors' => $validator->errors()
                ]);
            }

            $sites = Site::find($id);
            $sites->site = $request->site;
            $sites->url = $request->url;
            $sites->login_tag = $request->login_tag;
            $sites->login_search = $request->login_search;
            $sites->password_tag = $request->password_tag;
            $sites->password_search = $request->password_search;
            $sites->save();
            return response()->json([
                'fail' => false,
                'redirect_url' => url('FillingEditor/site')
            ]);
    }

    public function destroy($id){
        Site::destroy($id);
        return redirect('FillingEditor/site');
    }

}
