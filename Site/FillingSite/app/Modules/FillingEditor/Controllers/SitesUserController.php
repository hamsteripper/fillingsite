<?php

namespace App\Modules\FillingEditor\Controllers;

use App\Modules\FillingEditor\Models\DB\SitesUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class SitesUserController extends Controller {

    public function index(Request $request){

        $request->session()->put('searchUsername', $request->has('searchUsername') ? $request->get('searchUsername') : ($request->session()->has('searchUsername') ? $request->session()->get('searchUsername') : ''));

        $searchUsername = $request->session()->get('searchUsername');

        $sitesUser = new SitesUser();
        $sitesUser = $sitesUser
            ->where('username', 'like', '%' . $searchUsername . '%')
            ->orderBy('userID', 'desc')
            ->paginate(10);

        if ($request->ajax())
            return view("FillingEditor::sitesUser.index", compact('sitesUser'));
        else
            return view('FillingEditor::sitesUser.ajax', compact('sitesUser'));
    }

    // Создание новой строки
    public function create(Request $request){
        return view('FillingEditor::sitesUser.form', ['fromUrl' => $request->fromUrl]);
    }
    public function store(Request $request){
        $rules = [
            'username' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails())
            return response()->json([
                'fail' => true,
                'errors' => $validator->errors()
            ]);
        $sitesUser = new SitesUser();
        $sitesUser->username = $request->username;
        $sitesUser->save();
        return response()->json([
            'fail' => false,
            'redirect_url' => url($request->fromUrl)
        ]);
    }

    // Редактирование текущей строки
    public function edit(Request $request, $id){
        return view('FillingEditor::sitesUser.form', ['sitesUser' => SitesUser::find($id)]);
    }
    public function update(Request $request, $id){
        $rules = [
            'username' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails())
            return response()->json([
                'fail' => true,
                'errors' => $validator->errors()
            ]);
        $sitesUser = SitesUser::find($id);
        $sitesUser->username = $request->username;
        $sitesUser->save();
        return response()->json([
            'fail' => false,
            'redirect_url' => url('FillingEditor/sitesUser')
        ]);
    }

    public function destroy($id){
        SitesUser::destroy($id);
        return redirect('FillingEditor/sitesUser');
    }

}
