<?php

namespace App\Modules\FillingEditor\Controllers;

//use App\Modules\DevicesDatabase\Models\dh_type;
use App\Modules\FillingEditor\Models\DB\Site;
use App\Modules\FillingEditor\Models\DB\SitesUser;
use App\Modules\FillingEditor\Models\DB\SitesUsersSite;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
//use function Psy\debug;

class SitesUsersSiteController extends Controller {

    public function index(Request $request){

        $request->session()->put('searchUsername', $request->has('searchUsername') ? $request->get('searchUsername') : ($request->session()->has('searchUsername') ? $request->session()->get('searchUsername') : ''));
        $request->session()->put('searchSite', $request->has('searchSite') ? $request->get('searchSite') : ($request->session()->has('searchSite') ? $request->session()->get('searchSite') : ''));
        $request->session()->put('searchLogin', $request->has('searchLogin') ? $request->get('searchLogin') : ($request->session()->has('searchLogin') ? $request->session()->get('searchLogin') : ''));
        $request->session()->put('searchPassword', $request->has('searchPassword') ? $request->get('searchPassword') : ($request->session()->has('searchPassword') ? $request->session()->get('searchPassword') : ''));

        $searchUsername = $request->session()->get('searchUsername');
        $searchLogin = $request->session()->get('searchLogin');
        $searchPassword = $request->session()->get('searchPassword');
        $searchSite = $request->session()->get('searchSite');

        $sitesUsersSite = new SitesUsersSite();
        $sitesUsersSite = $sitesUsersSite
            ->where('login', 'like', '%' . $searchLogin . '%')
            ->where('password', 'like', '%' . $searchPassword . '%')
            ->with('sitesUser', 'site')
            ->WhereHas('sitesUser', function ($subquery) use ($searchUsername) {
                $subquery->where('username', 'like', "%{$searchUsername}%"); // Это относится к таблице с пользователями
            })
            ->WhereHas('site', function ($subquery) use ($searchSite) {
                $subquery->where('site', 'like', "%{$searchSite}%"); // Это относится к таблице с сайтами
            })
            ->orderBy('id', 'desc')
            ->paginate(10);

        if ($request->ajax())
            return view("FillingEditor::sitesUsersSite.index", compact('sitesUsersSite'));
        else
            return view('FillingEditor::sitesUsersSite.ajax', compact('sitesUsersSite'));
    }

    // Создание новой строки
    public function create(){
        $sitesUsers = SitesUser::all();
        $sites = Site::all();
        return view('FillingEditor::sitesUsersSite.form')->with('sites', $sites)->with('sitesUsers', $sitesUsers);
    }
    public function store(Request $request){
        $rules = [
            'user_id' => 'required',
            'site_id' => 'required',
            'login' => 'required',
            'password' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails())
            return response()->json([
                'fail' => true,
                'errors' => $validator->errors()
            ]);
        $sitesUsersSite = new SitesUsersSite();
        $sitesUsersSite->user_id = $request->user_id;
        $sitesUsersSite->site_id = $request->site_id;
        $sitesUsersSite->login = $request->login;
        $sitesUsersSite->password = $request->password;
        $sitesUsersSite->save();
        return response()->json([
            'fail' => false,
            'redirect_url' => url('FillingEditor/sitesUsersSite')
        ]);
    }

    // Редактирование текущей строки
    public function edit(Request $request, $id){
        $sitesUsers = SitesUser::all();
        $sites = Site::all();
        return view('FillingEditor::sitesUsersSite.form', ['SitesUsersSite' => SitesUsersSite::find($id)])->with('sites', $sites)->with('sitesUsers', $sitesUsers);
    }
    public function update(Request $request, $id){
        $rules = [
            'user_id' => 'required',
            'site_id' => 'required',
            'login' => 'required',
            'password' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails())
            return response()->json([
                'fail' => true,
                'errors' => $validator->errors()
            ]);
        $sitesUsersSite = SitesUsersSite::find($id);
        $sitesUsersSite->user_id = $request->user_id;
        $sitesUsersSite->site_id = $request->site_id;
        $sitesUsersSite->login = $request->login;
        $sitesUsersSite->password = $request->password;
        $sitesUsersSite->save();
        return response()->json([
            'fail' => false,
            'redirect_url' => url('FillingEditor/sitesUsersSite')
        ]);
    }

    public function destroy($id){
        SitesUsersSite::destroy($id);
        return redirect('FillingEditor/sitesUsersSite');
    }

}
