<?php

namespace App\Modules\FillingEditor\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MainController extends Controller {

    public function index(Request $request){
        return view("FillingEditor::main");
    }

}
