<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $users = DB::select('SELECT username, site, url, login_tag, login_search, password_tag, password_search, login, password
                                    FROM SitesUsers, Sites, SitesUsers_Sites
                                    WHERE SitesUsers.userID=SitesUsers_Sites.user_id
                                        AND Sites.siteID=SitesUsers_Sites.site_id
                                        AND SitesUsers.username = ?', ['xxx']);

        dump(['xxx']);
        // Преобразование в array
        $users = json_decode(json_encode($users), true);

        $user_sites = array();
        $all_sites = array();

        //$sites = array();
        //$config = array();

        foreach ($users as $user){
            dump($user);
            $user_s = array();
            $user_s["site"] = $user["site"];
            $user_s["url"] = $user["url"];
            $user_s["login"] = $user["login"];
            $user_s["password"] = $user["password"];
            $user_sites[] = $user_s;

            $site_c = array();
            $site_c['url'] = $user["url"];
            $site_c['loginTag'] = $user["login_tag"];
            $site_c['loginSearch'] = $user["login_search"];
            $site_c['passwordTag'] = $user["password_tag"];
            $site_c['passwordSearch'] = $user["password_search"];
            $all_sites[] = $site_c;

        }

        dump($user_sites);
        dump($all_sites);

        dump(["user_sites" => $user_sites, "all_sites" => $all_sites]);

    }
}
