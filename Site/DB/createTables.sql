use Filling;
CREATE TABLE `sites_users` (
    `userID` INT(11) NOT NULL AUTO_INCREMENT,
    `username` VARCHAR(255) NOT NULL,
    PRIMARY KEY (`userID`)
)
COMMENT='sites_users'
ENGINE=InnoDB
;
   
CREATE TABLE `sites` (
    `siteID` INT(11) NOT NULL AUTO_INCREMENT,
    `site` VARCHAR(255) NOT NULL,
    `url` VARCHAR(255) NOT NULL,
    `login_tag` VARCHAR(255) NOT NULL,
    `login_search` VARCHAR(255) NOT NULL,
    `password_tag` VARCHAR(255) NOT NULL,
    `password_search` VARCHAR(255) NOT NULL,
    PRIMARY KEY (`siteID`)
)
COMMENT='sites'
ENGINE=InnoDB
;
   
CREATE TABLE `sites_users_sites` (
    `user_id` INT(11) NOT NULL,
    `site_id` INT(11) NOT NULL,
    PRIMARY KEY (`user_id`, `site_id`),
    `login` VARCHAR(255) NOT NULL,
    `password` VARCHAR(255) NOT NULL,
    INDEX `user_id` (`user_id`),
    INDEX `site_id` (`site_id`),
    CONSTRAINT `FK_Site` FOREIGN KEY (`site_id`)
        REFERENCES `sites` (`siteID`) ON DELETE CASCADE,
    CONSTRAINT `FK_User` FOREIGN KEY (`user_id`)
        REFERENCES `sites_users` (`userID`) ON DELETE CASCADE
)
COMMENT='Link table between sites_users and sites'
ENGINE=InnoDB
;
