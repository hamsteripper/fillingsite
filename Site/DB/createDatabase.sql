CREATE database IF NOT EXISTS Filling;
drop user 'FillingUser'@'localhost';
CREATE USER IF NOT EXISTS 'FillingUser'@'localhost' IDENTIFIED BY 'FillingUser';
ALTER user 'FillingUser'@'localhost' identified with mysql_native_password by 'FillingUser';
GRANT ALL PRIVILEGES ON Filling.* TO 'FillingUser'@'%';
FLUSH PRIVILEGES;
use Filling;


