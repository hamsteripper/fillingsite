// Отправка id на запрос
chrome.extension.onMessage.addListener(function(request, sender, f_callback){
	if(request=='userID_request'){ //проверяется, от того ли окна и скрипта отправлено	
		var userId = localStorage.getItem('userId');
		var data = localStorage.getItem("userData");
		if(((userId === undefined) || (userId === "") || (userId === null)) && ((data === undefined) || (data === "") || (data === null))){
			// TODO Если id не задан
			console.log("userId не задан");	
			f_callback(data); //обратное сообщение not ok
		}else{
			console.log("userId отправляется");	
			f_callback(data); //обратное сообщение ok
		}
	}
});

// Очистка при старте
chrome.runtime.onStartup.addListener(function() {
    localStorage.clear();
});
// Очистка при закрытии
chrome.windows.onRemoved.addListener(function(windowId){
    localStorage.clear();
});

// Если открыта новая страница или обновлена
chrome.tabs.onUpdated.addListener(function (tabId, changeInfo, tab) {
	// Если страница полностью загружена
	if (changeInfo.status == "complete") {
		// Загружаем обработку на страницу
		chrome.tabs.executeScript(null, { file: 'jquery-3.6.0.min.js' });		
		chrome.tabs.executeScript(null, { file: 'inject.js' });						    	
	}
});
