/// Google ссылка
var googleURL = "1C09Q7GS9hrVlahvxEHBHZMtNHw2Vt4dtqzLjDohU7IU";
var db_host = '192.168.10.117';
var db_host_port = '3306';
var db_login = 'FillingUser';
var db_password = 'FillingUser';

// Заполнение полей расширения
function fillingInTheFields(){
    var data = localStorage.getItem("userData");
    data = JSON.parse(data);
    var fieldset = document.getElementById('fieldset');
    while (fieldset.firstChild) {
        fieldset.removeChild(fieldset.firstChild);
    }

    for( var val in data.userData ){
        var site = data.userData[val].site;
        var url = data.userData[val].url;
        var newbr = document.createElement('br');
		var newNode = document.createElement('a');
		newNode.setAttribute("href", url);
		newNode.setAttribute("target", "_blank");
		newNode.textContent = site;
		fieldset.appendChild(newNode);
		fieldset.appendChild(newbr);
    }
}

// Получение данных
function dataPreparation(userId, googleURL){
    try{
	    var jsonR = "";
	    var url = "https://spreadsheets.google.com/feeds/worksheets/"+googleURL+"/public/basic?alt=json"
	    // Получение параметров книги
	    var SpreadSheets = getJSON(url);
	    //console.log('SpreadSheets =', SpreadSheets);
	    var sheetUrl = getUserSheetUrl(userId, SpreadSheets);
	    console.log("sheetUrl = ",sheetUrl);
	    
	    // Получение данных страницы пользователя
	    var userData = getJSON(sheetUrl.userUrl);
	    console.log("userData1 = ", userData);
	    userData = userData['feed']['entry'];
	    // Получение структурированного массива данных +пользователя
	    var userData = userDataArrayCreation(userId, userData);
        console.log("userData2 = ", userData);
	    // Получение параматров страниц
	    var siteData = getJSON(sheetUrl.siteParameters);
	    siteData = siteData['feed']['entry'];
	    var sitesData = siteParametersArrayCreation(siteData);
        console.log("sitesData = ", sitesData);
	    
	    var AssocArray = {};
	    AssocArray["userData"] = userData;
	    AssocArray["siteData"] = sitesData;
	    localStorage.setItem("userData", JSON.stringify(AssocArray));
    }catch (e) {
    	clearButtonFunction();
    }
}

// Получение данных
function dataPreparation_fromDB(userId, db_host, db_host_port, db_login, db_password) {
    console.log("dataPreparation_fromDB");

    var data = new FormData();
    data.append('username', userId);
    var response = null;
    var xhr = new XMLHttpRequest();
    xhr.open('post', "http://filling.spectroom-studio.ru/GetSitesInfo", false);
    xhr.onload = function() {
        var status = xhr.status;
        if (status === 200) {
            response = JSON.parse(xhr.response);
            console.log(response);
        } else {
            response = JSON.parse(xhr.response);
            console.log(response);
        }
    };
    xhr.send(data);

    var AssocArray = {};
    AssocArray["userData"] = response["userData"];
    AssocArray["siteData"] = response["siteData"];
    localStorage.setItem("userData", JSON.stringify(AssocArray));
    //return response;
}


// Обработчик нажатия на клавишу авторизации
document.getElementById("AutorizationButton").addEventListener("click", autorizationButtonFunction);
function autorizationButtonFunction(){
    var userId = document.getElementById('userId').value;
    if((userId !== undefined) && (userId !== "") && (userId !== null)){
        localStorage.setItem('userId', userId);
        //dataPreparation(userId, googleURL);
        dataPreparation_fromDB( userId, db_host, db_host_port, db_login, db_password);
        fillingInTheFields();
    }else{
    	clearButtonFunction();
    }
}

// Обработчик нажатия на клавишу авторизации
//document.getElementById("ClearButton").addEventListener("click", clearButtonFunction);
function clearButtonFunction(){

	document.getElementById('userId').value = "";
	document.getElementById("fieldset").outerHTML='';

	localStorage.setItem('userId', "");
        localStorage.setItem("userData", "");
        
        localStorage.removeItem('userId');
        localStorage.removeItem('userData');
        
        window.close();
}


// Загрузка страницы
function runOnStart() {
    // Получение id пользователя из сессии
    var userId = localStorage.getItem('userId');
    if((userId !== undefined) && (userId !== "") && (userId !== null)){
        document.getElementById('userId').value = userId;
        fillingInTheFields();
    }else{
    	document.getElementById('userId').value = "";
    }
}

// Обработка при загрузке страницы
if(document.readyState !== 'loading') {
    runOnStart();
}
else {
    document.addEventListener('DOMContentLoaded', function () {
        runOnStart()
    });
}
