function fillingLogin(loginTag, login){
	console.log("loginTag");
	loginTag.type="password";
	if( document.execCommand ){
		console.log("execCommand");
		loginTag.focus();
		document.execCommand("insertHTML", false, login);
	}
	loginTag.value = login;
}

function fillingPassword(passwordTag, password){
	console.log("passwordTag");
	passwordTag.type="password";
	if( document.execCommand ){
		console.log("execCommand");
		passwordTag.focus();
		document.execCommand("insertHTML", false, password);
	}
	passwordTag.value = password;
}

function filling(userData, siteData){
	
	var arrayTags = siteData.loginTag.split('|');
	var tag = null;
	for (var key in arrayTags) {
		var arrayT = arrayTags[key].split(',');
		// If id
		if(arrayT.length === 1 && arrayT[0] === "id"){
			var loginNameTag = document.getElementById(siteData.loginSearch);
			fillingLogin(loginNameTag, userData.login);
		// If name
		}else if(arrayT.length === 2 && arrayT[0] === "name"){
			if(tag !== null){
				var loginNameTag = tag.document.getElementsByName(siteData.loginSearch)[arrayT[1]];
				fillingLogin(loginNameTag, userData.login);
			}else{
				var loginNameTag = document.getElementsByName(siteData.loginSearch)[arrayT[1]];
				fillingLogin(loginNameTag, userData.login);
			}
		// If frame
		}else if(arrayT.length === 2 && arrayT[0] === "frame"){
			if(tag === null){
				tag = window.frames[arrayT[1]];
			}else{
				console.log("frame found");
			}
		}
	}
	
	var arrayTags = siteData.passwordTag.split('|');
	var tag = null;
	for (var key in arrayTags) {
		var arrayT = arrayTags[key].split(',');
		// If id
		if(arrayT.length === 1 && arrayT[0] === "id"){
			var passwordNameTag = document.getElementById(siteData.passwordSearch);
			fillingPassword(passwordNameTag, userData.password);
		// If name
		}else if(arrayT.length === 2 && arrayT[0] === "name"){
			if(tag !== null){
				var passwordNameTag = tag.document.getElementsByName(siteData.passwordSearch)[arrayT[1]];
				fillingPassword(passwordNameTag, userData.password);
			}else{
				var passwordNameTag = document.getElementsByName(siteData.passwordSearch)[arrayT[1]];
				fillingPassword(passwordNameTag, userData.password);
			}
		// If frame
		}else if(arrayT.length === 2 && arrayT[0] === "frame"){
			if(tag === null){
				tag = window.frames[arrayT[1]];
				console.log("first frame found");
			}else{
				console.log("frame found");
			}
		}
	}
}


// Заполнение полей на сайте
function fillingInTheFields(data){
	data = JSON.parse(data);
	var currentUrl = document.URL;
	var userData = 	data.userData;
	var siteData = 	data.siteData;
	
	for( var userVal in userData ){
		if(userData[userVal].url === currentUrl){
			for( var siteVal in siteData ){
				if(siteData[siteVal].url === currentUrl){
					/*console.log(
						" site = ",userData[userVal].site,
						"url = ",userData[userVal].url,
						" login = ",userData[userVal].login,
						" password = ",userData[userVal].password);
					console.log(
						" url = ",siteData[siteVal].url,
						" loginTag = ",siteData[siteVal].loginTag,
						" loginSearch = ",siteData[siteVal].loginSearch,
						" passwordTag = ",siteData[siteVal].passwordTag,
						" passwordSearch = ",siteData[siteVal].passwordSearch);
						*/
					
					filling(userData[userVal], siteData[siteVal]);
				}
			}	
		}
	}
};



// Стартовая функция
if(document.readyState !== 'loading'){
    // Получение ID пользователя
    chrome.extension.sendMessage('userID_request', function(data){
		if((data === undefined) || (data === "") || (data === null)){
			console.log("userId не задан");
		}else{
			setTimeout(fillingInTheFields, 2000, data);
		}
    });
}
