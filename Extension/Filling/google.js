// Обработчик запросов
function getJSON(url) {
	var response = null;
	var xhr = new XMLHttpRequest();
	xhr.open('GET', url, false);
	xhr.onload = function() {
		var status = xhr.status;
		if (status === 200) {
			response = JSON.parse(xhr.response);
		} else {
			response = JSON.parse(xhr.response);
		}
	};
	xhr.send();
	return response;
};

// Создание массива параметров сайта
function siteParametersArrayCreation(data){
	console.log(data);
	var maxCol = 0;
	var maxRow = 0;
	
	var urlCol = -1;
	var loginTagCol = -1;
	var loginSearchCol = -1;
	var passwordTagCol = -1;
	var passwordSearchCol = -1;
	
	data.forEach(function(item, i, data){
		if(item['gs$cell']['$t'] === "url"){
			urlCol = Number(item['gs$cell']['col'])-1;
		}else if(item['gs$cell']['$t'] === "login_tag"){
			loginTagCol = Number(item['gs$cell']['col'])-1;
		}else if(item['gs$cell']['$t'] === "login_search"){
			loginSearchCol = Number(item['gs$cell']['col'])-1;
		}else if(item['gs$cell']['$t'] === "password_tag"){
			passwordTagCol = Number(item['gs$cell']['col'])-1;
		}else if(item['gs$cell']['$t'] === "password_search"){
			passwordSearchCol = Number(item['gs$cell']['col'])-1;
		}
	
		if(maxCol < Number(item['gs$cell']['col']))
			maxCol = Number(item['gs$cell']['col']);
		if(maxRow < Number(item['gs$cell']['row']))
			maxRow = Number(item['gs$cell']['row']);
	});
	
	if((urlCol === -1) || (loginTagCol === -1) || (loginSearchCol === -1) || (passwordTagCol === -1) || (passwordSearchCol === -1)){
		console.log("error не все данные есть");
		return;
	}
	
	var array = [];
	for(var i = 1; i < maxRow; i++){
		var url = data[i*maxCol+urlCol]['gs$cell']['$t'];
		var loginTag = data[i*maxCol+loginTagCol]['gs$cell']['$t'];
		var loginSearch = data[i*maxCol+loginSearchCol]['gs$cell']['$t'];
		var passwordTag = data[i*maxCol+passwordTagCol]['gs$cell']['$t'];
		var passwordSearch = data[i*maxCol+passwordSearchCol]['gs$cell']['$t'];
		var a = {};
		a['url'] = url;
		a['loginTag'] = loginTag;
		a['loginSearch'] = loginSearch;
		a['passwordTag'] = passwordTag;
		a['passwordSearch'] = passwordSearch;

		array.push(a);
	}
	return array;
}

// Создание массива данных
function userDataArrayCreation(userId, data){
	
	var maxCol = 0;
	var maxRow = 0;
	var urlCol = -1;
	var loginCol = -1;
	var passwordCol = -1;
	var siteCol = -1;
	data.forEach(function(item, i, data){
		if(item['gs$cell']['$t'] === "url"){
			urlCol = Number(item['gs$cell']['col'])-1;
		}else if(item['gs$cell']['$t'] === "login"){
			loginCol = Number(item['gs$cell']['col'])-1;
		}else if(item['gs$cell']['$t'] === "password"){
			passwordCol = Number(item['gs$cell']['col'])-1;
		}else if(item['gs$cell']['$t'] === "site"){
			siteCol = Number(item['gs$cell']['col'])-1;
		}
		
		if(maxCol < Number(item['gs$cell']['col']))
			maxCol = Number(item['gs$cell']['col']);
		if(maxRow < Number(item['gs$cell']['row']))
			maxRow = Number(item['gs$cell']['row']);
	});
	
	if((urlCol === -1) || (loginCol === -1) || (passwordCol === -1) || (siteCol === -1)){
		console.log("error не все данные есть");
		return;
	}
	
	var array = [];
	for(var i = 1; i < maxRow; i++){
		var site = data[i*maxCol+siteCol]['gs$cell']['$t'];
		var url = data[i*maxCol+urlCol]['gs$cell']['$t'];
		var login = data[i*maxCol+loginCol]['gs$cell']['$t'];
		var password = data[i*maxCol+passwordCol]['gs$cell']['$t'];
		var a = {};
		a['site'] = site;
		a['url'] = url;
		a['login'] = login;
		a['password'] = password;
		array.push(a);
	}

	return array;
}

// Получить описание книги
function getUserSheetUrl(userId, data){
	
	var userSheetUrl = null;
	var siteParametersUrl = null;
	var response = {};
	
	// Ищем страницу пользователя и страницу параметров
	data = data['feed']['entry'];
	data.forEach(function(item, i, data){
		// Находим страницу пользователя
		if(item['title']['$t'] === userId){
			userSheetUrl = item['link'];
		}
		// Находим страницу параметров
		if(item['title']['$t'] === "site_parameters"){
			siteParametersUrl = item['link'];
		}
	});
	
	// Ищем url на страницу пользователя
	userSheetUrl.forEach(function(item, i, userSheetUrl) {
		//console.log(item['href']);
		var href = item['href'];
		// Получение данных ячеек выбраной страницы
		if(href.indexOf('https://spreadsheets.google.com/feeds/cells') != -1){
			// Замена basic на full
			var href = href.replace('basic', 'full');
			response["userUrl"] = href + "?alt=json";			
		}
	});
	
	siteParametersUrl.forEach(function(item, i, userSheetUrl) {
		//console.log(item['href']);
		var href = item['href'];
		// Получение данных ячеек выбраной страницы
		if(href.indexOf('https://spreadsheets.google.com/feeds/cells') != -1){
			// Замена basic на full
			var href = href.replace('basic', 'full');
			response["siteParameters"] = href + "?alt=json";
		}
	});
	
	return response;
}
